require "rails_helper"

RSpec.feature "Button click reveals text", type: :feature do
  scenario "User visits page and clicks orange button", js: true do
    visit "orange/banana/"
    expect(page).to have_content("orange")
    expect(page).to_not have_content("You glad I didn't say banana!")

    find(".orange-click").click
    sleep(100)
    expect(page).to have_content("doop")
  end
end
